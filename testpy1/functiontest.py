def avg(first, *rest):
    return float((first+sum(rest)))/(1+len(rest))


def any_args(*args, **kwargs):
    key_val = [' %s="%s"' % item for item in kwargs.items()]
    attr_str = ''.join(key_val)
    for s in key_val:
        print s
    for i in args:
        print i

print avg(1, 2, 3, 4)
dic1 = dict()
dic1['one'] = 1
dic1['two'] = 2
print any_args('item', 'Albatross', key1='one', key2='two', key3='three')