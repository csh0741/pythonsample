import json

session_content = '''
                    {
                    "SessionUserInterface":"RA2",
                    "SessionUserFirstName":"Sam",
                    "SessionUUID":"e91r1no53sp4lj2j620a534833",
                    "SessCompany":{"off":["4"],"on":["1"]},
                    "SessionUID":"1003",
                    "SessionAccountName":"James.Harmon",
                    "SessionPassword":"32b4be1db057d11835fa875e9dea1a6b",
                    "SessionRole":"Nurse",
                    "SessionRoleForLogin":"staff",
                    "SessionClass":"UP,RA",
                    "SessionLogin":"Pass",
                    "SessionAuthority":false,
                    "SessionSystemAdmin":true,
                    "SessionAdmin":true,
                    "SuperAdmin":true,
                    "DeviceName":"",
                    "AppleID":"",
                    "NickName":"",
                    "DeviceNameOrigin":"",
                    "OriginalAccountName":"sam.chung",
                    "OriginalUID":"954",
                    "SessionWorkFacilityID":"7",
                    "SessionWorkFacilitySuspend":"No",
                    "SessionFacilityName":"Cypress Creek Hospital ",
                    "SessionFacilityAbreviation":"CCH ",
                    "SessionManager":true,
                    "SessionMultiFacilityCount":true
                    }
                    '''

session_dic = json.loads(session_content)
function_list = {}

if session_dic["SessCompany"]["on"][0] == "1":
    ra_patient_forms = {}
    ra_patient_forms['RA_Patient_Form'] = {}

    #function_list.append("")

print session_dic