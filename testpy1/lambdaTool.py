# encoding: utf-8
import functools
from functools import reduce


Sum = lambda x, y: x + y
print(Sum(3, 4))

print(reduce(Sum, [1, 2, 3, 4, 5])/5)


def fahrenheit(T):
    return (float(9)/5)*T + 32


def celsius(T):
    return (float(5)/9)*(T-32)

temperatures = [36.5, 37, 37.5, 38, 39]
F = map(fahrenheit, temperatures)
C = map(celsius, F)
print(F)
print(C)


LambF = lambda x: ((float(9)/5) * x + 32)
LambC = lambda x: (float(5)/9)*(x-32)
F = map(LambF, temperatures)
C = map(LambC, F)
print(F)
print(C)

LambFilter = lambda x: x % 2
print(filter(LambFilter, [0, 1, 2, 4, 8, 7, 5, 9, 11]))

GetMax = lambda x, y: x if (x > y) else y
print(reduce(GetMax, [0, 1, 2, 4, 8, 7, 5, 9, 11]))
print(max([0, 1, 2, 3, 4, 5, 6, 7, 8]))
