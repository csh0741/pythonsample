class A:
    def a(self):
        print 'a'

class B:
    def a(self):
        print 'b'


class C(A, B):
    pass

c = C()
c.a()
