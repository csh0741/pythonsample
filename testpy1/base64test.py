import base64
import json
from phpserialize import *
import pickle

encoded_data = base64.b64encode(b'28187980401')
print(encoded_data)

str_code = 'YTo1OntpOjA7YToxOntzOjg6IlF1ZXN0aW9uIjtzOjI2OiJkYXRlIG9mIGJpcnRoIChNTS9ERC9ZWVlZKSI7fWk6MTthOjI6e3M6MTA6IkNoZWNrVmFsdWUiO3M6MzoiWWVzIjtzOjg6IlF1ZXN0aW9uIjtzOjE2OiJjZWxscGhvbmUgbnVtYmVyIjt9aToyO2E6MTp7czo4OiJRdWVzdGlvbiI7czoxNzoiaG9tZSBwaG9uZSBudW1iZXIiO31pOjM7YToxOntzOjg6IlF1ZXN0aW9uIjtzOjE2OiJtYWlsaW5nIHppcCBjb2RlIjt9aTo0O2E6Mjp7czoxMDoiQ2hlY2tWYWx1ZSI7czozOiJZZXMiO3M6ODoiUXVlc3Rpb24iO3M6MTU6ImVtcGxveWVlIG51bWJlciI7fX0='
decoded_data = base64.b64decode(str_code)
print(decoded_data)
pair = loads(decoded_data)
print (pair)
pair2 = unserialize(decoded_data)
question_list = []
for i in pair:
    if 'CheckValue' in pair[i] and pair[i]['CheckValue'] == 'Yes':
        pair[i]['answer'] = ''
        question_list.append(dict((k.lower(), v) for k, v in pair[i].iteritems()))

str_code = 'YToyOntpOjA7czoyNjoiZGF0ZSBvZiBiaXJ0aCAoTU0vREQvWVlZWSkiO2k6MTtzOjE2OiJtYWlsaW5nIHppcCBjb2RlIjt9'
decoded_data = base64.b64decode(str_code)
pair = loads(decoded_data)
print (pair)
builder_string = []
for i in pair:
    builder_string.append(pair[i])

encoded_data = base64.b64encode(dumps(builder_string))


def get_browser(user_agent):
        browser = "Unknown Browser";
        browser_array = dict()
        browser_array[r'edge/'] = 'Edge'
        browser_array[r'msie/'] = 'Internet Explorer'
        browser_array[r'crios/'] = 'Chrome'
        browser_array[r'chrome/'] = 'Chrome'
        browser_array[r'firefox/'] = 'Firefox'
        browser_array[r'safari/'] = 'Safari'
        browser_array[r'opera/'] = 'Opera'
        browser_array[r'netscape/'] = 'Netscape'
        browser_array[r'maxthon/'] = 'Maxthon'
        browser_array[r'konqueror/'] = 'Konqueror'
        browser_array[r'mobile/'] = 'Handheld Browser'
        import re
        result = dict((k.lower(), v) for k, v in browser_array.iteritems() if re.search(k, user_agent,re.M|re.I))
        for i in result:
            browser = result[i]
        return browser


def get_os_platform(user_agent):
        os = "Unknown OS Platform";
        platform_array = dict()
        platform_array['windows nt 10.0'] = 'Windows 10'
        platform_array['windows nt 6.3'] = 'Windows 8.1'
        platform_array['windows nt 6.2'] = 'Windows 8'
        platform_array['windows nt 6.1'] = 'Windows 7'
        platform_array['windows nt 6.0'] = 'Windows Vista'
        platform_array['windows nt 5.2'] = 'Windows Server 2003/XP x64'
        platform_array['windows nt 5.1'] = 'Windows XP'
        platform_array['windows xp'] = 'Windows XP'
        platform_array['windows nt 5.0'] = 'Windows 2000'
        platform_array['windows me'] = 'Windows ME'
        platform_array['win98'] = 'Windows 98'
        platform_array['win95'] = 'Windows 95'
        platform_array['win16'] = 'Windows 3.11'
        platform_array['macintosh|mac os x'] = 'Mac OS X'
        platform_array['mac_powerpc'] = 'Mac OS 9'
        platform_array['linux'] = 'Linux'
        platform_array['ubuntu'] = 'Ubuntu'
        platform_array['iphone'] = 'iPhone'
        platform_array['ipod'] = 'iPod'
        platform_array['ipad'] = 'iPad'
        platform_array['android'] = 'Android'
        platform_array['blackberry'] = 'BlackBerry'
        platform_array['webos'] = 'Mobile'
        import re
        result = dict((k.lower(), v) for k, v in platform_array.iteritems() if re.search(k, user_agent,re.M|re.I))
        for i in result:
            os = result[i]
        return os

print get_browser('Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/42.0.2311.135 Safari/537.36 Edge/12.10240')
print get_os_platform('Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/42.0.2311.135 Safari/537.36 Edge/12.10240')