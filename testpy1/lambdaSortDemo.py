# encoding: utf-8

#python lambda sorter

#list in dict sort demo
dic = {
    'a': [1, 2, 3],
    'b': [2, 1, 3],
    'c': [3, 1, 2],
}
print(sorted(dic, key=lambda k: dic[k][0]))
# ['a', 'b', 'c']
print(sorted(dic, key=lambda k: dic[k][1]))
# ['b', 'c', 'a']
print(sorted(dic, key=lambda k: dic[k][2]))
# ['c', 'b', 'a']
print(sorted(dic, key=lambda k: dic[k][0], reverse=True))
# ['c', 'b', 'a']



#dict in list sort demo

lis = [
    {'x': 3, 'y': 2, 'z': 1},
    {'x': 2, 'y': 1, 'z': 3},
    {'x': 1, 'y': 3, 'z': 2},
]
print(sorted(lis, key=lambda k: k['x']))
# [{'z': 2, 'x': 1, 'y': 3}, {'z': 3, 'x': 2, 'y': 1}, {'z': 1, 'x': 3, 'y': 2}]
print(sorted(lis, key=lambda k: k['y']))
# [{'z': 3, 'x': 2, 'y': 1}, {'z': 1, 'x': 3, 'y': 2}, {'z': 2, 'x': 1, 'y': 3}]
print(sorted(lis, key=lambda k: k['z']))
# [{'z': 1, 'x': 3, 'y': 2}, {'z': 2, 'x': 1, 'y': 3}, {'z': 3, 'x': 2, 'y': 1}]
print(sorted(lis, key=lambda k: k['x'], reverse=True))
# [{'z': 1, 'x': 3, 'y': 2}, {'z': 3, 'x': 2, 'y': 1}, {'z': 2, 'x': 1, 'y': 3}]
max0 = max(lis, key=lambda item: item['x'])
min0 = min(lis, key=lambda item: item['x'])
print("get max dic['x'] in list")
print(max0)
print("get min dic['x'] in list")
print(min0)
max0 = max(lis, key=lambda(item): item['x'])
min0 = min(lis, key=lambda(item): item['x'])
print("get max dic['x'] in list lambda(item)")
print(max0)
print("get min dic['x'] in list lambda(item)")
print(min0)


#dict in dict nest sort demo
dic = {
    'a': {'x': 3, 'y': 2, 'z': 1},
    'b': {'x': 2, 'y': 1, 'z': 3},
    'c': {'x': 3, 'y': 6, 'z': 18},
    'd': {'x': 1, 'y': 3, 'z': 2}
}
print(sorted(dic, key=lambda k: dic[k]['x']))
# ['c', 'b', 'a']
print(sorted(dic, key=lambda k: dic[k]['y']))
# ['b', 'a', 'c']
print(sorted(dic, key=lambda k: dic[k]['z']))
# ['a', 'c', 'b']
print(sorted(dic, key=lambda k: dic[k]['x'], reverse=True))
# ['a', 'b', 'c']

#max dic key by value
print('取得巢狀dict 物件內層的x key最大值')
max1 = max(dic, key=lambda(item): dic[item]['x'])
print(max1)
print(dic[max1])
print('取得巢狀dict 物件內層的x key合計值')
print(dic.values())
sum1 = reduce(lambda x, value: x + value['x'], dic.values(), 0)
print(sum1)
print('string to dict')
print(reduce(lambda output, current: output.update({current: ord(current)}) or output, 'abbbcc', {}))
print(reduce(lambda output, current: dict(output.items() + [(current, ord(current))]), 'abc', {}))
print('string to tuple list')
print(reduce(lambda output, current: output + [(current, ord(current))], 'abc', []))
print('tuple list to dict')
dict2 = dict([('a', 1), ('b', 2)])
dict2 = dict(dict2.items() + [('c', 3)])
dict2.update({'c': 2})
print(dict2)

#list in list sort demo
lis = [[4, 2, 9], [1, 5, 6], [7, 8, 3]]
print(sorted(lis, key=lambda k: k[0]))
# [[1, 5, 6], [4, 2, 9], [7, 8, 3]]
print(sorted(lis, key=lambda k: k[1]))
# [[4, 2, 9], [1, 5, 6], [7, 8, 3]]
print(sorted(lis, key=lambda k: k[2]))
# [[7, 8, 3], [1, 5, 6], [4, 2, 9]]
print(sorted(lis, key=lambda k: k[0], reverse=True))
# [[7, 8, 3], [4, 2, 9], [1, 5, 6]]

#dict value demo
dic = {'c': 1, 'b': 2, 'a': 3}
print(sorted(dic, key=lambda k: dic[k]))
# ['c', 'b', 'a']
print(sorted(dic, key=lambda k: dic[k], reverse=True))
# ['a', 'b', 'c']

#dict key demo
dic = {'c': 1, 'b': 2, 'a': 3}
print(sorted(dic))
# ['a', 'b', 'c']
print(sorted(dic, reverse=True))
# ['c', 'b', 'a']
print(reduce(lambda x, value: x + value, dic.values(), 0))
max = max(dic, key=lambda item: dic[item])
min = min(dic, key=lambda(item): dic[item])
print(dic[max])
print(dic[min])