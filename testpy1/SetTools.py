# encoding: utf-8
import functools
from functools import reduce


print("test list")
NewList = [1, 5, 9, 8, 7, 9]
print(NewList)
print(type(NewList))
print("test list extend")
List2 = [2, 4, 6, 8, 10, 12]
NewList.extend(List2)
print(NewList)

print("test set")
NewSet = {1, 5, 9, 8, 9, 7, 2}
print(NewSet)
print(type(NewSet))
print("set to list")
print(list(NewSet))
print("list to set")
print(set(NewList))


print("test tuple")
NewTuple = (1, 3, 5, 7, 9)
print(NewTuple)
print(type(NewTuple))


print("dict demo")
tel = {'jack': 4098, 'sape': 4139}
print(tel)
print(type(tel))
tel['guido'] = 4127
del tel['sape']
tel['irv'] = 4127
print(tel)
for k, v in tel.items():
    print(k, v)
DicToList = list(tel)
print(DicToList)

print("list zip demo")
questions = ['name', 'quest', 'favorite color']
answers = ['lancelot', 'the holy grail', 'blue']
for q, a in zip(questions, answers):
    print('What is your {0}?  It is {1}.'.format(q, a))

print("dict in list")
DictInList = [{'Name': 'John', 'Pts': 29, 'Club': 'CHI'}, {'Name': 'Joba', 'Pts': 29, 'Club': 'CHI'}
              , {'Name': 'Mike', 'Pts': 32, 'Club': 'LAL'}, {'Name': 'Cousey', 'Pts': 25, 'Club': 'LAL'}
              , {'Name': 'Scott', 'Pts': 18.1, 'Club': 'LAL'}, {'Name': 'Tompson', 'Pts': 22.3, 'Club': 'GSW'}]
print(DictInList)
NewList = [dic['Name'] for dic in DictInList]
NewList.sort()
print(NewList)

print("dict in list do demo")
NewList = list(filter(lambda x: x['Pts'] > 23 and x['Club'] == 'LAL', DictInList))
print(NewList)

print("dict in list sort demo")
a = sorted(NewList, key=lambda x: x["Pts"], reverse=True)
print(type(a))
print(a)
