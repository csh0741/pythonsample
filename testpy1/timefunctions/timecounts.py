from datetime import *
import time
import os
import ConfigParser

a = datetime.now()
print "a.datetime ==> %s" %a

b = a+timedelta(seconds=3)
print "b.datetime ==> %s" %b
print "type(b) ==> %s" % type(b)

print "y=%s" % b.year
print "M=%s" % b.month
print "d=%s" % b.day
print "H=%s" % b.hour
print "m=%s" % b.minute
print "s=%s" % b.second

new_date = datetime(2015, 9, 1)
new_date = new_date.replace(hour=7, minute=1, second=1)
print "new_date ==> %s" % (new_date + timedelta(seconds=1))
print "new_date ==> last day of month %s" % (new_date + timedelta(days=-1))
print "new_date days of month: %s" % (new_date + timedelta(days=-1)).day
print "time.mktime(new_date.timetuple()) ==> %s" % time.mktime(new_date.timetuple())
print "(b - new_date).days ==> %s" % (b - new_date).days
print "(new_date - b).days ==> %s" % (new_date - b).days
print "(b - new_date).seconds ==> %s" % (b - new_date).seconds
print "(new_date - b).seconds ==> %s" % (new_date - b).seconds
print new_date.strftime('%Y-%m-%d %H:%M:%S')
print new_date.strftime('%H:%M:%S')

print new_date.strftime('%Y-%m-%d %H:%M:%S') + new_date.strftime('%H:%M:%S')

print a, b
print "time.mktime(a.timetuple()) ==> %s" %time.mktime(a.timetuple())
print "time.mktime(b.timetuple()) ==> %s" %time.mktime(b.timetuple())
result = time.mktime(a.timetuple()) - time.mktime(b.timetuple())
print "time.mktime(a.timetuple()) - time.mktime(b.timetuple()) ==> %s" % result
print datetime.strptime("01:05AM","%I:%M%p")
print datetime.strptime("12:05AM","%I:%M%p")
print time.strptime("01:05AM","%I:%M%p")
print (5/8.+40)*2.54