from pubnub import Pubnub


class MessageFactory:
    def __init__(self):
        print "init messages"

    @staticmethod
    def create(notifier_type='pubnub'):
        if notifier_type == 'pubnub':
            return PubNub()
        else:
            return PubNub()


class PubNub:
    """
     use pubnub service to publish messages
    """
    # publish_key = "pub-c-8d7dc07f-38d1-4ec6-87d3-7315618d3ca5"
    # subscribe_key = "sub-c-204df6ea-85a9-11e4-89f8-02ee2ddab7fe"
    publish_key = "pub-c-5b511ce8-f52d-4150-9c25-a2850807ed0e"
    subscribe_key = "sub-c-a86661a0-97c6-11e4-a07f-02ee2ddab7fe"
    messageQ = None

    def __init__(self):
        self.messageQ = Pubnub(
            publish_key=self.publish_key,
            subscribe_key=self.subscribe_key)

    def publish(self, channel_name, data={}):
        self.messageQ.publish(
            channel_name,
            data,
            callback=_callback,
            error=_error)

    def subscribe(self, channel_name):
        self.messageQ.subscribe(
            channel_name,
            callback=_callback,
            error=_error)


def apns(message):
    notifier = MessageFactory().create()
    notifier.publish('on_call_providers')


def _callback(message):
    print(message)


def _error(message):
    print("ERROR : " + str(message))


def notify_on_call_providers():
    notifier = MessageFactory().create()
    notifier.publish('on_call_providers')


def notify_waiting_room():
    notifier = MessageFactory().create()
    notifier.publish('waiting_room')


def notify_waiting_room_notification(message_receiver, notification_type, message_body):
    if notification_type == 'away_status':
        channel = 'waiting_room_notification_%s' % message_receiver
    else:
        channel = 'waiting_room_notification'
    data = {notification_type: message_body}
    notifier = MessageFactory().create()
    notifier.publish(channel, data)
