# encoding: utf-8
import functools
from functools import reduce

def max(m, n):
    return m if m > n else n

print(max(10, 3))  # 顯示 10

new_max = lambda m, n: m if m > n else n
print(new_max(10, 3))  # 顯示 10


g = lambda x: x**2
print(g(8))


def make_incrementor (n):
    return lambda x: x + n




f = make_incrementor(2)
g = make_incrementor(6)
print(f(42))
print(g(42))
print(make_incrementor(22)(33))

foo = [2, 18, 9, 22, 17, 24, 8, 12, 27]
table = [
    {'name': 'tom', 'age': 22},
    {'name': 'bill', 'age': 23},
    {'name': 'john', 'age': 21},
    {'name': 'lin', 'age': 24},
    {'name': 'Johnson', 'age': 25},
    {'name': 'Tomlin', 'age': 27}
]
#print(sum(v["age"], v in table))

print("進行foo的過濾filter")
results = list(filter(lambda x: x % 3 == 0, foo))
print(results)  #將原來的組合過濾為3的倍數
print("進行foo的處理map =>* 2 + 10")
print(list(map(lambda x: x * 2 + 10, foo)))   #將原來的組合乘2加10
print("進行foo的合計 functools.reduce")
print(reduce(lambda x, y: x + y, foo))  #將原來的組合處理合計
print(sum(foo))


nums = list(range(2, 50))
print("所有的nums值:")
print(nums)
print("過濾掉偶數 保留奇數:")
print(list(filter(lambda x: x % 2, nums)))
print("過濾掉奇數 保留偶數:")
print(list(filter(lambda x: x % 2 == 0, nums)))
print("開始過濾nums值:")
for i in range(2, 8):
    print("過濾 %d的倍數" %i)
    new_nums = list(filter(lambda x: x == i or x % i == 0, nums))
    print(new_nums)
#print(nums)


print("列印2~7")
for i in range(2, 8):
    print(i)


def add(n1):
    def func(n2):
        return n1 + n2
    return func

print("進行累加")
print(add(1)(2))  # 顯示 3


def outer():
    x = 10

    def inner():
        print(x)
    print("執行內部函式inner()")
    inner()
    x = 20
    print("將inner()當作回傳的實體")
    return inner

print("先進行函式的指派")
'''因為指派outer時會先執行outer()內的inner()，所以會print預設的x=10，而return inner ，是將內部函式指派給f'''
f = outer()
print("執行函式")
f()


print("測試range正序反序 1=>11，每次+1")
for i in range(1, 11, 1):
    print(i)

print("測試range正序反序 10=>0，每次-1")
for i in range(10, 0, -1):
    print(i)