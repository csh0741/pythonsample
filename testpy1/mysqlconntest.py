import MySQLdb

conn = MySQLdb.connect(host="drs-mysql-testdb1.cyveltfrg9ci.us-west-1.rds.amazonaws.com",user="root",
                  passwd="0000",db="TMP")
cur = conn.cursor(MySQLdb.cursors.DictCursor)

cur.execute('select * from fax_content where fax_list_id=277')

field_names = [i[0] for i in cur.description]
for field in field_names:
    print field

row = cur.fetchone()

while row:
    print row["content"]
    row = cur.fetchone()

# if you call execute() with one argument, you can use % sign as usual
# (it loses its special meaning).

conn.close()
