import json

def get_function_array():
    root_list = list()
    '''
    root_list.append({"id": 1, "name": "Waiting Room", "parent_id": 0, "navigate_url": "functions/unassign/inputList.php?inputType=PreAdmit", "sort_number": 0, "role_number": 1})
    root_list.append({"id": 2,"name":"RA Patient Form","parent_id":0,"navigate_url":"", "sort_number": 0})
    root_list.append({"id": 3,"name":"Telehealth Standard Form","parent_id":2,"navigate_url":"functions/standard_form/standardForm.php", "sort_number": 0, "role_number": 1})
    root_list.append({"id": 4,"name":"MOT Standard Form","parent_id":2,"navigate_url":"functions/standard_form_mot/standardForm.php", "sort_number": 0, "role_number": 1})
    root_list.append({"id": 5,"name":"Code White Standard Form","parent_id":2,"navigate_url":"functions/code_white_standard_form/standardForm.php", "sort_number": 0, "role_number": 1})
    root_list.append({"id": 6,"name":"TeleMAT Standard Form","parent_id":2,"navigate_url":"functions/telemat_standard_form/standardForm.php", "sort_number": 0, "role_number": 1})
    root_list.append({"id": 7,"name":"Order Form","parent_id":2,"navigate_url":"functions/order_form/Protocol.php", "sort_number": 0, "role_number": 1})
    root_list.append({"id": 8,"name":"Form Field Notes","parent_id":2,"navigate_url":"functions/field_notes/index.php", "sort_number": 0, "role_number": 1})
    root_list.append({"id": 9,"name":"New Patient","parent_id":2,"navigate_url":"functions/medical_forms/input.php?inputtype=AdmitStaff", "sort_number": 0, "role_number": 1})
    root_list.append({"id": 10,"name":"Facility Profile","parent_id":0,"navigate_url":"functions/user/facility_profile.php", "sort_number": 0, "role_number": 1})
    root_list.append({"id": 11,"name":"Admin","parent_id":0,"navigate_url":"", "sort_number": 0, "role_number": 1})
    root_list.append({"id": 12,"name":"Policy","parent_id":11,"navigate_url":"functions/policy/index.php", "sort_number": 0, "role_number": 1})
    root_list.append({"id": 13,"name":"Billing Feeds","parent_id":11,"navigate_url":"functions/billing_feeds/index.php", "sort_number": 0, "role_number": 1})
    root_list.append({"id": 14,"name":"Issues","parent_id":11,"navigate_url":"functions/issues/index.php", "sort_number": 0, "role_number": 1})
    root_list.append({"id": 15,"name":"View Changes","parent_id":11,"navigate_url":"functions/web_log/index.php", "sort_number": 0, "role_number": 1})
    root_list.append({"id": 16,"name":"Records Time Log","parent_id":11,"navigate_url":"functions/records_timelog/index.php", "sort_number": 0, "role_number": 1})
    root_list.append({"id": 17,"name":"Attending Physician","parent_id":11,"navigate_url":"functions/attending_physician/index.php", "sort_number": 0, "role_number": 1})
    root_list.append({"id": 18,"name":"Device Management","parent_id":11,"navigate_url":"functions/device_management/index.php", "sort_number": 0, "role_number": 1})
    root_list.append({"id": 19,"name":"Class","parent_id":11,"navigate_url":"functions/class/index.php", "sort_number": 0, "role_number": 1})
    root_list.append({"id": 20,"name":"Billing","parent_id":0,"navigate_url":"", "sort_number": 0, "role_number": 1})
    root_list.append({"id": 21,"name":"Billing Worklist","parent_id":20,"navigate_url":"http://54.183.156.72:9780", "sort_number": 0, "role_number": 1})
    root_list.append({"id": 22,"name":"Facesheet Worklist","parent_id":20,"navigate_url":"functions/facesheet_worklist/index.php", "sort_number": 0, "role_number": 1})
    root_list.append({"id": 23,"name":"Facesheet Management","parent_id":20,"navigate_url":"functions/facesheet_management/index.php", "sort_number": 0, "role_number": 1})
    root_list.append({"id": 24,"name":"Billing Report","parent_id":20,"navigate_url":"functions/billing_report2/index.php", "sort_number": 0, "role_number": 1})
    root_list.append({"id": 25,"name":"Records","parent_id":20,"navigate_url":"functions/billing/index.php", "sort_number": 0, "role_number": 1})
    root_list.append({"id": 26,"name":"Scheduling","parent_id":0,"navigate_url":"", "sort_number": 0, "role_number": 1})
    root_list.append({"id": 27,"name":"Redianswer Credentialing","parent_id":26,"navigate_url":"functions/user/RediAnswerCredentialing.php", "sort_number": 0, "role_number": 1})
    root_list.append({"id": 28,"name":"Set Privileges","parent_id":26,"navigate_url":"functions/privileges/index.php", "sort_number": 0, "role_number": 1})
    root_list.append({"id": 29,"name":"Availability","parent_id":26,"navigate_url":"functions/availability/Staff.php", "sort_number": 0, "role_number": 1})
    root_list.append({"id": 30,"name":"Set Default","parent_id":26,"navigate_url":"functions/default_value/choice.php", "sort_number": 0, "role_number": 1})
    root_list.append({"id": 31,"name":"Scheduling","parent_id":26,"navigate_url":"functions/data_table_new/index.php", "sort_number": 0, "role_number": 1})
    root_list.append({"id": 32,"name":"Calendar","parent_id":0,"navigate_url":"", "sort_number": 0, "role_number": 1})
    root_list.append({"id": 33,"name":"Calendar","parent_id":32,"navigate_url":"functions/facility_report/Report.php", "sort_number": 0, "role_number": 1})
    root_list.append({"id": 34,"name":"Reminder","parent_id":32,"navigate_url":"functions/reminder/index.php", "sort_number": 0, "role_number": 1})
    root_list.append({"id": 35,"name":"Payment","parent_id":0,"navigate_url":"", "sort_number": 0, "role_number": 1})
    root_list.append({"id": 36,"name":"Account","parent_id":35,"navigate_url":"functions/payment/index.php", "sort_number": 0, "role_number": 1})
    root_list.append({"id": 37,"name":"Default Mail","parent_id":35,"navigate_url":"functions/default_mail/MailTemplate.php", "sort_number": 0, "role_number": 1})
    root_list.append({"id": 38,"name":"Profile","parent_id":0,"navigate_url":"", "sort_number": 0, "role_number": 1})
    root_list.append({"id": 39,"name":"Group Profile","parent_id":38,"navigate_url":"functions/company/company_list.php", "sort_number": 0, "role_number": 1})
    root_list.append({"id": 40,"name":"User Profile","parent_id":38,"navigate_url":"functions/user/index.php", "sort_number": 0, "role_number": 1})
    root_list.append({"id": 41,"name":"User Profile","parent_id":38,"navigate_url":"functions/company_user_manager/index.php", "sort_number": 0, "role_number": 1})
    root_list.append({"id": 42,"name":"Facility Profile","parent_id":38,"navigate_url":"functions/user/facilityprofile_index.php", "sort_number": 0, "role_number": 1})
    root_list.append({"id": 43,"name":"Office Check in Report","parent_id":38,"navigate_url":"functions/office_checkin_report/index.php", "sort_number": 0, "role_number": 1})
    root_list.append({"id": 44,"name":"Publish Schedule","parent_id":26,"navigate_url":"functions/publish/index.php", "sort_number": 0,"role_number":1})
    root_list.append({"id": 45,"name":"Switch User","parent_id":0,"navigate_url":"functions/alllogin/index.php", "sort_number": 0,"role_number":1})
    '''
    root_list.append({"id": 46, "name": "Add H&P Patient", "parent_id": 0, "navigate_url": "functions/add_hp_patient/index.php","role_number": 2, "sort_number": 0})
    root_list.append({"id": 47, "name": "Records", "parent_id": 0, "navigate_url": "functions/records/FacilityRecord.php","role_number": 2, "sort_number": 5})
    root_list.append({"id": 48, "name": "Registration", "parent_id": 0, "navigate_url": "functions/new_user/index.php","role_number": 2, "sort_number": 10})
    root_list.append({"id": 49, "name": "Member Management", "parent_id": 0, "navigate_url": "functions/user/index.php","role_number": 2, "sort_number": 15})
    root_list.append({"id": 50, "name": "My Profile", "parent_id": 0, "navigate_url": "functions/user/NurseSelf.php","role_number": 2, "sort_number": 20})
    root_list.append({"id": 51, "name": "Calendar", "parent_id": 0, "navigate_url": "functions/facility_report/Report.php","role_number": 2, "sort_number": 25})
    root_list.append({"id": 52, "name": "Waiting Room", "parent_id": 0, "navigate_url": "functions/waiting_room/inputFacilityList.php","role_number":2, "sort_number": 30})
    root_list.append({"id": 53, "name": "Order Settings", "parent_id": 0, "navigate_url": "functions/order_form/PrintActiveOrders.php","role_number":2, "sort_number": 35})
    root_list.append({"id": 54, "name": "Billing Report", "parent_id": 0, "navigate_url": "functions/billing_report2/index_201407.php","role_number":2, "sort_number": 40})
    root_list.append({"id": 55, "name": "Waiting Room", "parent_id": 0, "navigate_url": "functions/waiting_room/inputList.php", "role_number": 3, "sort_number": 0})
    root_list.append({"id": 56, "name": "Policy", "parent_id": 0, "navigate_url": "functions/policy/index.php","role_number":3, "sort_number": 5})
    root_list.append({"id": 57, "name": "H&P", "parent_id": 0, "navigate_url": "functions/hp/index.php", "role_number": 3, "sort_number": 10})
    root_list.append({"id": 58, "name": "User Profile", "parent_id": 0, "navigate_url": "functions/new_user/index.php", "role_number": 3, "sort_number": 15})
    root_list.append({"id": 59, "name": "Member Management", "parent_id": 0, "navigate_url": "functions/user/index.php", "role_number": 3, "sort_number": 20})
    root_list.append({"id": 60, "name": "My Profile", "parent_id": 0, "navigate_url": "functions/user/NurseSelf.php", "role_number": 3, "sort_number": 25})
    root_list.append({"id": 61, "name": "Calendar", "parent_id": 0, "navigate_url": "functions/facility_report/Report.php", "role_number": 3, "sort_number": 30})
    root_list.append({"id": 62, "name": "Order Settings", "parent_id": 0, "navigate_url": "functions/order_form/PrintActiveOrders.php", "role_number": 3, "sort_number": 35})
    root_list.append({"id": 63,
                      "name": "Billing Report",
                      "parent_id": 0,
                      "navigate_url": "functions/billing_report2/index_201407.php",
                      "sort_number": 40,
                      "role_number": 3
    })
    root_list.append({"id": 64, "name": "Facility Profile", "parent_id": 0, "navigate_url": "functions/billing_report2/index_201407.php", "sort_number": 22, "role_number": 2})
    root_list.append({"id": 65, "name": "Redianswer Credentialing", "parent_id": 0, "navigate_url": "functions/billing_report2/index_201407.php", "sort_number": 23, "role_number": 2})
    return root_list


nurse_permission = filter(lambda x: x['role_number'] == 2, get_function_array())

print len(nurse_permission)



