import pyodbc
from dataobject.dbconn_method_1 import *


import dataobject.dbconn
connection_str = """
Driver={SQL Server Native Client 11.0};
Server=127.0.0.1\SQLEXPRESS;
Database=JanchenSysDB;
Trusted_Connection=No;
Uid=sa;
Pwd=1234;
"""

conn = pyodbc.connect(connection_str)
cur = conn.cursor()
listresult = []
tool = dataobject.dbconn.conntool()
tool2 = dataobject.dbconn.conntool()
tool3 = dataobject.dbconn.conntool()
tool.first_name = 'john'
tool.set_show(1)
tool2.first_name = 'johnny'
tool2.set_show(2)
tool3.first_name = 'alex'
tool3.set_show(3)

listresult.append(tool)
listresult.append(tool2)
listresult.append(tool3)
#use lambda to sort by first_name
newlist = sorted(listresult, key=lambda x: x.first_name, reverse=True)

#tool2.getName()
#tool.getName()

print(len(listresult))

for to in listresult:
    to.getName()
for to in newlist:
    to.getName()


dicresult = dict()
dicresult["one"] = "123455"
dicresult["one"] = "123456"
dicresult["two"] = "2426455"
print(dicresult["one"])
print(dicresult["two"])

for (k, v) in dicresult.iteritems():
    print("%s => %s" % (k, v))


cur.execute('SELECT * FROM TB_FACTORY WHERE FACT_ID>? ', 150)

field_names = [i[0] for i in cur.description]
for field in field_names:
    print field


def dict_fetchall(cursor):
    "Returns all rows from a cursor as a dict"
    desc = cursor.description
    return [
        dict(zip([col[0] for col in desc], row))
        for row in cursor.fetchall()
    ]


result =dict_fetchall(cur)

for row in result:
    #print "ID=%d, Name=%s" % (row["FACT_ID"], row["FACT_NAME"])
    for (k, v) in row.iteritems():
        print(" %s = %s " % (k,v))
    #row = cur.fetchone()


row = cur.fetchone()
while row:
    print "ID=%d, Name=%s" % (row.FACT_ID, row.FACT_NAME)
    row = cur.fetchone()

# if you call execute() with one argument, you can use % sign as usual
# (it loses its special meaning).

conn.close()
