from abc import ABCMeta,abstractmethod

class ColorFish:
    __metaclass__ = ABCMeta

    @abstractmethod
    def getFishColor(self):
        pass
