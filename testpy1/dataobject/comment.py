# encoding: utf-8
from datetime import datetime

class Comment(object):

    _email=''
    _content=''
    _created=datetime.now

    def __init__(self, email, content, created=None):
        self._email = email
        self._content = content
        self._created = created or datetime.now()
        self.submail=email  #這邊若有指定self 層級的變數 則視為該類別已經建立的變數之一

    @property
    def email(self):
        return self.submail

    @property
    def content(self):
        return self._content

    @property
    def created(self):
        return self._created


comment = Comment(email='csh0741@gmail.com',content='teaeqre',created=datetime.now())
print comment.email
