import base64
import phpserialize

question_content = '''YTo1OntpOjA7YToxOntzOjg6IlF1ZXN0aW9uIjtzOjI2OiJkYXRlIG9mIGJpcnRoIChNTS9ER
                      C9ZWVlZKSI7fWk6MTthOjI6e3M6MTA6IkNoZWNrVmFsdWUiO3M6MzoiWWVzIjtzOjg6IlF1ZX
                      N0aW9uIjtzOjE2OiJjZWxscGhvbmUgbnVtYmVyIjt9aToyO2E6MTp7czo4OiJRdWVzdGlvbiI
                      7czoxNzoiaG9tZSBwaG9uZSBudW1iZXIiO31pOjM7YToxOntzOjg6IlF1ZXN0aW9uIjtzOjE2
                      OiJtYWlsaW5nIHppcCBjb2RlIjt9aTo0O2E6Mjp7czoxMDoiQ2hlY2tWYWx1ZSI7czozOiJZZ
                      XMiO3M6ODoiUXVlc3Rpb24iO3M6MTU6ImVtcGxveWVlIG51bWJlciI7fX0='''

pair = phpserialize.loads(base64.b64decode(question_content))
encode_content = base64.b64encode(phpserialize.dumps(pair))
print encode_content
print pair
print pair[0]
print '========================================'
for i in pair:
    print pair[i]
print '========================================'
for k, v in pair.iteritems():
    print k
    print v
print '========================================'
new_pair = {k: v for k, v in pair.iteritems() if "CheckValue" in v and v['CheckValue'] == 'Yes'}
for k, v in new_pair.iteritems():
    print k
    print v
print '========================================'
question_list = [v for k, v in new_pair.iteritems()]
print question_list
print '========================================'
question_list = [n_v for n_k, n_v in
                 {k: v for k, v in pair.iteritems() if "CheckValue" in v and v['CheckValue'] == 'Yes'}.iteritems()]

print question_list
# function_lists = [{k: v for k, v in d.iteritems() if k
#                          in ['id', 'name', 'navigate_url', 'icon', 'parent_id', 'childs']}
#                          for d in function_lists]
print '========================================'
keys = ['can_edit',
        'can_del',
        'can_chat',
        'can_del_comment',
        'can_select_device',
        'can_make_appointment',
        'can_just_video']

permissions = dict(zip(keys, [True for k in keys]))
print permissions

new_permissions = [True for k in keys]
print new_permissions



fax_content = 'YToyOntpOjA7YTozOntzOjQ6Ik5hbWUiO3M6MjoiRUQiO3M6NjoiTnVtYmVyIjtzOjExOiIxOTM2MjkxNDM3OCI7' \
              'czo3OiJEZWZhdWx0IjtpOjE7fWk6MTthOjM6e3M6NDoiTmFtZSI7czozOiJJQ1UiO3M6NjoiTnVtYmVyIjtzOjEx' \
              'OiIxOTM2MjkxNDI2MyI7czo3OiJEZWZhdWx0IjtpOjA7fX0='

pair = phpserialize.loads(base64.b64decode(fax_content))
print pair
print pair[0]
print '========================================'
for i in pair:
    print pair[i]
print '========================================'

content_string = base64.b64decode(encode_content)