a = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h']
print ('Before', a)
a[2:7] = [99, 22, 14]
print ('After', a)


b = a[:]
print ('b', b)
b = a
a[:] = [101, 102, 103]
print('b', b)

b = a[:]
a[:] = [104, 105, 106, 107, 108]
print('a', a)
print('b', b)

from datetime import datetime, timedelta
from dateutil import tz, parser
from time import mktime
import pytz

tpe_date = datetime.now()
utc_date = datetime.utcnow()
print utc_date
#chicago = pytz.timezone('America/Chicago')
#print type(chicago)
#print utc_date.replace(tzinfo=chicago)

to_zone = tz.gettz('America/Chicago')
print type(to_zone)
center = datetime.now(tz=to_zone)
print center
print center.hour
print center
print center + timedelta(minutes=900)

s = datetime.strptime("8/29/15 12:00AM", "%m/%d/%y %I:%M%p")
print s
print s + timedelta(minutes=1440)

dt = parser.parse("8/29/2015 08:00pm")
print dt

time_default = '10'
print int(15) + 12

dt = parser.parse("22/9/2015 08:00pm")
print dt
print dt + timedelta(seconds=0.2)

dt = parser.parse("5/10/2015 23:59")
print dt


date_list =[{'start_time': datetime.now(), 'end_time': datetime.now() + timedelta(minutes=10)},
            {'start_time': datetime.now(), 'end_time': datetime.now() + timedelta(minutes=10)},
            {'start_time': datetime.now(), 'end_time': datetime.now() + timedelta(minutes=10)},
            {'start_time': datetime.now(), 'end_time': datetime.now() + timedelta(minutes=10)},
            {'start_time': datetime.now(), 'end_time': datetime.now() + timedelta(minutes=10)},
            {'start_time': datetime.now(), 'end_time': datetime.now() + timedelta(minutes=10)},
            {'start_time': datetime.now(), 'end_time': datetime.now() + timedelta(minutes=10)},
            {'start_time': datetime.now(), 'end_time': datetime.now() + timedelta(minutes=10)},
            {'start_time': datetime.now(), 'end_time': datetime.now() + timedelta(minutes=10)},
            {'start_time': datetime.now(), 'end_time': datetime.now() + timedelta(minutes=10)},
            {'start_time': datetime.now(), 'end_time': datetime.now() + timedelta(minutes=10)},
            {'start_time': datetime.now(), 'end_time': datetime.now() + timedelta(minutes=10)},
            {'start_time': datetime.now(), 'end_time': datetime.now() + timedelta(minutes=10)}]
#date_list2 = [i['start_time'] + timedelta(minutes=5) for i in date_list]
#print date_list2
for i, kv in enumerate(date_list, 1):
    kv['start_time'] += timedelta(minutes=i)
    #kv['range'] = mktime(kv['end_time'].timetuple()) - mktime(kv['start_time'].timetuple())


max_time = max(date_list, key=lambda(item): mktime(item['end_time'].timetuple())
                                            - mktime(item['start_time'].timetuple()))
min_time = min(date_list, key=lambda(item): mktime(item['end_time'].timetuple())
                                            - mktime(item['start_time'].timetuple()))

max = mktime(max_time['end_time'].timetuple()) - mktime(max_time['start_time'].timetuple())
print max
min = mktime(min_time['end_time'].timetuple()) - mktime(min_time['start_time'].timetuple())
print min