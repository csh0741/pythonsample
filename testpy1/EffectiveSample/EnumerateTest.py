flavor_list = ['google', 'yahoo', 'sina', 'pchome']

for i, flavor in enumerate(flavor_list):
    print ('%d: %s' % (i + 1, flavor))

print '--break--'

for i, flavor in enumerate(flavor_list, 1):
    print ('%d: %s' % (i, flavor))

names = ['Cecilia', 'Lise', 'Marie']
letters = [len(n) for n in names]
print names
print letters
longest_name = None
max_letters = 0

for i, name in enumerate(names):
    count = letters[i]
    if count > max_letters:
        longest_name = name
        max_letters = count

print longest_name
print max_letters

for name, count in zip(names, letters):
    if count > max_letters:
        longest_name = name
        max_letters = count

print longest_name
print max_letters
