dict_content = {'1': 1, '2': 2, '3': 3, '4': 4, '5': 5}
print dict_content

content_list = list(dict_content.values())
print content_list


new_pair = {k: v for k, v in dict_content.iteritems() if v < 3}
content_list = list(new_pair.keys())
print content_list

print new_pair


print "12934134134".replace('4', '5')


dict_content = {"a": "1234567890",
                "b": "web",
                "t0": "change",
                "s": "1293732723",
                "tm": "1444275975656",
                "u": "215",
                "v": "1958610665",
                "n0": "input",
                "y0": "@div;#main;.nurse-waitroom;|@div;#appContent;."
                      "ng-scope;|@div;#requestAccess;.fade;.in;.modal;."
                      "scrolling;|@div;.modal-dialog;.modal-lg;|@div;."
                      "modal-content;|@form;.ng-dirty;.ng-valid;.ng-vali"
                      "d-parse;|@div;.modal-body;|@div;.mb20;.row;|@div;"
                      ".col-md-12;.col-sm-12;.col-xs-12;.ml13;|@div;.form-group;"
                      "|@div;.col-sm-6;.col-xs-6;|@input;.form-control;.ng-dirty;"
                      ".ng-untouched;.ng-valid;.ng-valid-parse;.search_;|",
                "c0": "form-control search_ ng-untouched ng-valid ng-dirty ng-valid-parse"}

stamp = 1444277234
from datetime import datetime
print datetime.fromtimestamp(stamp).strftime('%Y-%m-%d %H:%M:%S')
stamp = 1444276458
print datetime.fromtimestamp(stamp).strftime('%Y-%m-%d %H:%M:%S')
tm = datetime.fromtimestamp(stamp)
print tm

from dateutil import tz

to_zone = tz.gettz('America/Chicago')
center = tm.now(tz=to_zone)
print center
