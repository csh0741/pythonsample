# encoding: utf-8
import json
from datetime import datetime
from datetime import timedelta
from time import *

foo = [2, 18, 9, 22, 17, 24, 8, 12, 27]
print filter(lambda x: x % 3 == 0, foo)

print max(foo)


people = [('Barack', 'Obama'), ('Oprah', 'Winfrey'), ('Mahatma', 'Gandhi')]
print max(people, key=lambda x: x[0])

print max(foo, key=lambda x: x)


print sorted(foo, key=lambda x: x, reverse=False)
print sorted(foo, key=lambda x: x, reverse=True)
print max(foo, key=lambda x: x)

test_str = "<a href=javascript://void(0) onclick=InputQuickNote(\'1234\',\'Patient&nbsp;is&nbsp;ready&nbsp;now.\')>CLICK</a>"

source_str = "My name is Sam @Click@"


def processing_request_str(str_source):
    search = ['\"', "\'", '"', "'", '´', '`']
    replace_key = ['&quot;', '&#39;', '&quot;', '&#39;', '&#180;', '&#96;']
    for i in search:
        str_source = str_source.replace(i, replace_key[search.index(i)])
    return str_source

new_str = source_str.replace("@Click@", test_str)

print new_str
new_str = processing_request_str(new_str)
print new_str
count1 = 9424345
print type(count1)
print type(str(count1))
print str(count1)


#json_str = '''{"tool":"FaceTime","id":"ap.id.8002@drsays.org","passwd":"0000"}'''
json_str = '''{"tool":"FaceTime","id":"8888","passwd":"0000"}'''
json_dic = dict.fromkeys(['tool', 'id', 'passwd'])
json_dic = json.loads(json_str)

print json_dic['tool']
print json_dic['id']

import re
regex = re.compile('^(?:ap.id.)(?P<deviceid>[0-9]+)(?:@drsays.org)$')
find_list = regex.findall(json_dic['id'])
print find_list

print json_dic['passwd']

print type(json_dic).__name__

json_dic = json.loads('1234')
print type(json_dic).__name__


def test_func(md_name, service_type="", start_time=datetime.now()):
    print md_name
    print start_time
    print service_type


test_func(1234, start_time=datetime.now().replace(hour=5))
test_func(1234, "999")

arr = ['1234', 'sam', '9781', '921']
print tuple(arr)
print bool('12345')
print bool('')


from classtest import *

student = Student()
student.update(student_id=1, class_id=2, student_name="John")
print student.class_id
print student.student_id
print student.student_name