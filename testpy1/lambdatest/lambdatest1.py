# encoding: utf-8
def max(m, n):
    return m if m > n else n

print(max(10, 3))  # 顯示 10


max = lambda m, n: m if m > n else n
print(max(10, 3))  # 顯示 10

g = lambda x: x**2
print(g(8))

def make_incrementor (n):
    return lambda x: x + n
f = make_incrementor(2)
g = make_incrementor(6)
print(f(42))
print(g(42))
print(make_incrementor(2)(3))
print make_incrementor(22)(33)