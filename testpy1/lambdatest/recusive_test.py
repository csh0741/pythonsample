import json
from collections import OrderedDict


class Permission():

    def __init__(self):
        pass

    @staticmethod
    def get_function_array():
        root_list = list()
        root_list.append({"id": 1, "name": "Waiting Room", "icon": "icon-staff-waiting-room", "parent_id": 0, "navigate_url": "/v2/waitting_room/staff", "sort_number": 0, "role_number": 1})
        root_list.append({"id": 2,"name":"RA Patient Form", "icon": "icon-staff-ra-patient-form", "parent_id":0,"navigate_url":"", "sort_number": 5, "role_number" : 1})
        root_list.append({"id": 3,"name":"Telehealth Standard Form", "icon": "", "parent_id":2,"navigate_url":"functions/standard_form/standardForm.php", "sort_number": 10, "role_number": 1})
        root_list.append({"id": 4,"name":"MOT Standard Form", "icon": "", "parent_id":2,"navigate_url":"functions/standard_form_mot/standardForm.php", "sort_number": 15, "role_number": 1})
        root_list.append({"id": 5,"name":"Code White Standard Form", "icon": "" ,"parent_id":2,"navigate_url":"functions/code_white_standard_form/standardForm.php", "sort_number": 20, "role_number": 1})
        root_list.append({"id": 6,"name":"TeleMAT Standard Form", "icon": "", "parent_id":2,"navigate_url":"functions/telemat_standard_form/standardForm.php", "sort_number": 25, "role_number": 1})
        root_list.append({"id": 7,"name":"Order Form", "icon": "", "parent_id":2,"navigate_url":"functions/order_form/Protocol.php", "sort_number": 30, "role_number": 1})
        root_list.append({"id": 8,"name":"Form Field Notes", "icon": "", "parent_id":2,"navigate_url":"functions/field_notes/index.php", "sort_number": 35, "role_number": 1})
        root_list.append({"id": 9,"name":"New Patient", "icon": "", "parent_id":2,"navigate_url":"functions/medical_forms/input.php?inputtype=AdmitStaff", "sort_number": 40, "role_number": 1})
        root_list.append({"id": 10,"name":"Facility Profile", "icon": "icon-facility-facility-profile", "parent_id":0,"navigate_url":"functions/user/facility_profile.php", "sort_number": 45, "role_number": 1})
        root_list.append({"id": 11,"name":"Admin", "icon": "icon-staff-setting", "parent_id":0,"navigate_url":"", "sort_number": 50, "role_number": 1})
        root_list.append({"id": 12,"name":"Policy", "icon": "", "parent_id":11,"navigate_url":"functions/policy/index.php", "sort_number": 55, "role_number": 1})
        root_list.append({"id": 13,"name":"Billing Feeds", "icon": "" ,"parent_id":11,"navigate_url":"functions/billing_feeds/index.php", "sort_number": 60, "role_number": 1})
        root_list.append({"id": 14,"name":"Issues", "icon": "", "parent_id":11,"navigate_url":"functions/issues/index.php", "sort_number": 65, "role_number": 1})
        root_list.append({"id": 15,"name":"View Changes", "icon": "", "parent_id":11,"navigate_url":"functions/web_log/index.php", "sort_number": 70, "role_number": 1})
        root_list.append({"id": 16,"name":"Records Time Log", "icon": "", "parent_id":11,"navigate_url":"functions/records_timelog/index.php", "sort_number": 75, "role_number": 1})
        root_list.append({"id": 17,"name":"Attending Physician", "icon": "", "parent_id":11,"navigate_url":"functions/attending_physician/index.php", "sort_number": 80, "role_number": 1})
        root_list.append({"id": 18,"name":"Device Management", "icon": "", "parent_id":11,"navigate_url":"functions/device_management/index.php", "sort_number": 85, "role_number": 1})
        root_list.append({"id": 19,"name":"Class", "icon": "", "parent_id":11,"navigate_url":"functions/class/index.php", "sort_number": 90, "role_number": 1})
        root_list.append({"id": 20,"name":"Billing", "icon": "icon-staff-billing", "parent_id":0,"navigate_url":"", "sort_number": 95, "role_number": 1})
        root_list.append({"id": 21,"name":"Billing Worklist", "icon": "", "parent_id":20,"navigate_url":"http://54.183.156.72:9780", "sort_number": 100, "role_number": 1})
        root_list.append({"id": 22,"name":"Facesheet Worklist", "icon": "", "parent_id":20,"navigate_url":"functions/facesheet_worklist/index.php", "sort_number": 105, "role_number": 1})
        root_list.append({"id": 23,"name":"Facesheet Management", "icon": "", "parent_id":20,"navigate_url":"functions/facesheet_management/index.php", "sort_number": 110, "role_number": 1})
        root_list.append({"id": 24,"name":"Billing Report", "icon": "", "parent_id":20,"navigate_url":"functions/billing_report2/index.php", "sort_number": 115, "role_number": 1})
        root_list.append({"id": 25,"name":"Records", "icon": "", "parent_id":20,"navigate_url":"functions/billing/index.php", "sort_number": 120, "role_number": 1})
        root_list.append({"id": 26,"name":"Scheduling", "icon": "icon-staff-scheduling", "parent_id":0,"navigate_url":"", "sort_number": 125, "role_number": 1})
        root_list.append({"id": 27,"name":"Redianswer Credentialing", "icon": "", "parent_id":26,"navigate_url":"functions/user/RediAnswerCredentialing.php", "sort_number": 130, "role_number": 1})
        root_list.append({"id": 28,"name":"Set Privileges", "icon": "", "parent_id":26,"navigate_url":"functions/privileges/index.php", "sort_number": 135, "role_number": 1})
        root_list.append({"id": 29,"name":"Availability", "icon": "", "parent_id":26,"navigate_url":"functions/availability/Staff.php", "sort_number": 140, "role_number": 1})
        root_list.append({"id": 30,"name":"Set Default", "icon": "", "parent_id":26,"navigate_url":"functions/default_value/choice.php", "sort_number": 145, "role_number": 1})
        root_list.append({"id": 31,"name":"Scheduling", "icon": "", "parent_id":26,"navigate_url":"functions/data_table_new/index.php", "sort_number": 150, "role_number": 1})
        root_list.append({"id": 32,"name":"Calendar", "icon": "icon-staff-calendar", "parent_id":0,"navigate_url":"", "sort_number": 155, "role_number": 1})
        root_list.append({"id": 33,"name":"Calendar", "icon": "", "parent_id":32,"navigate_url":"functions/facility_report/Report.php", "sort_number": 160, "role_number": 1})
        root_list.append({"id": 34,"name":"Reminder","parent_id":32,"navigate_url":"functions/reminder/index.php", "sort_number": 165, "role_number": 1})
        root_list.append({"id": 35,"name":"Payment", "icon": "icon-staff-payment", "parent_id":0,"navigate_url":"", "sort_number": 170, "role_number": 1})
        root_list.append({"id": 36,"name":"Account", "icon": "", "parent_id":35,"navigate_url":"functions/payment/index.php", "sort_number": 175, "role_number": 1})
        root_list.append({"id": 37,"name":"Default Mail", "icon": "", "parent_id":35,"navigate_url":"functions/default_mail/MailTemplate.php", "sort_number": 180, "role_number": 1})
        root_list.append({"id": 38,"name":"Profile", "icon": "icon-staff-profile", "parent_id":0,"navigate_url":"", "sort_number": 185, "role_number": 1})
        root_list.append({"id": 39,"name":"Group Profile", "icon": "", "parent_id":38,"navigate_url":"functions/company/company_list.php", "sort_number": 190, "role_number": 1})
        root_list.append({"id": 40,"name":"User Profile", "icon": "", "parent_id":38,"navigate_url":"functions/user/index.php", "sort_number": 195, "role_number": 1})
        root_list.append({"id": 41,"name":"User Profile","parent_id":38,"navigate_url":"functions/company_user_manager/index.php", "sort_number": 200, "role_number": 1})
        root_list.append({"id": 42,"name":"Facility Profile", "icon": "", "parent_id":38,"navigate_url":"functions/user/facilityprofile_index.php", "sort_number": 210, "role_number": 1})
        root_list.append({"id": 43,"name":"Office Check in Report", "icon": "", "parent_id":38,"navigate_url":"functions/office_checkin_report/index.php", "sort_number": 215, "role_number": 1})
        root_list.append({"id": 44,"name":"Publish Schedule", "icon": "", "parent_id":26,"navigate_url":"functions/publish/index.php", "sort_number": 220, "role_number": 1})
        root_list.append({"id": 45,"name":"Switch User", "icon": "icon-provider-switch", "parent_id":0,"navigate_url":"functions/alllogin/index.php", "sort_number": 225, "role_number": 1})

        root_list.append({"id": 46, "name": "Add H&P Patient", "icon": "icon-facility-add-hp-patient", "parent_id": 0, "navigate_url": "functions/add_hp_patient/index.php", "sort_number": 0, "role_number": 2})
        root_list.append({"id": 47, "name": "Records", "icon": "icon-facility-records", "parent_id": 0, "navigate_url": "functions/records/FacilityRecord.php", "sort_number": 5, "role_number": 2})
        root_list.append({"id": 48, "name": "Registration", "icon": "icon-facility-registration", "parent_id": 0, "navigate_url": "functions/new_user/index.php", "sort_number": 10, "role_number": 2})
        root_list.append({"id": 49, "name": "Member Management", "icon": "icon-facility-member-management", "parent_id": 0, "navigate_url": "functions/user/index.php", "sort_number": 15, "role_number": 2})
        root_list.append({"id": 50, "name": "My Profile", "icon": "icon-facility-my-profile", "parent_id": 0, "navigate_url": "functions/user/NurseSelf.php", "sort_number": 20, "role_number": 2})
        root_list.append({"id": 51, "name": "Calendar", "icon": "icon-facility-calendar", "parent_id": 0, "navigate_url": "functions/facility_report/Report.php", "sort_number": 25, "role_number": 2})
        root_list.append({"id": 52, "name": "Waiting Room", "icon": "icon-facility-waiting-room", "parent_id": 0, "navigate_url": "v2/waitting_room/nurse", "sort_number": 30, "role_number": 2})
        root_list.append({"id": 53, "name": "Order Settings", "icon": "icon-facility-order-setting", "parent_id": 0, "navigate_url": "functions/order_form/PrintActiveOrders.php", "sort_number": 35, "role_number": 2})
        root_list.append({"id": 54, "name": "Billing Report", "icon": "icon-facility-billing-report", "parent_id": 0, "navigate_url": "functions/billing_report2/index_201407.php", "sort_number": 40, "role_number": 2})

        root_list.append({"id": 55, "name": "Waiting Room", "icon": "icon-provider-waiting-room", "parent_id": 0, "navigate_url": "v2/waitting_room/provider", "sort_number": 0, "role_number": 3})
        root_list.append({"id": 56, "name": "Policy", "icon": "icon-provider-poilcy", "parent_id": 0, "navigate_url": "functions/policy/index.php", "sort_number": 5, "role_number": 3})
        root_list.append({"id": 57, "name": "H&P", "icon": "", "parent_id": 0, "navigate_url": "functions/hp/index.php", "sort_number": 10, "role_number": 3})
        root_list.append({"id": 58, "name": "User Profile", "icon": "icon-provider-user-profile", "parent_id": 0, "navigate_url": "functions/user/index.php", "sort_number": 15, "role_number": 3})
        root_list.append({"id": 59, "name": "Calendar", "icon": "icon-provider-calendar", "parent_id": 0, "navigate_url": "functions/doctor_report/Report.php", "sort_number": 20, "role_number": 3})
        root_list.append({"id": 60, "name": "Verification", "icon": "icon-provider-verfication", "parent_id": 0, "navigate_url": "functions/payment/ViewStatement.php", "sort_number": 25, "role_number": 3})
        root_list.append({"id": 61, "name": "Account", "icon": "icon-provider-account", "parent_id": 0, "navigate_url": "functions/payment/DoctorAccount.php", "sort_number": 30, "role_number": 3})
        root_list.append({"id": 62, "name": "Post Availability", "icon": "", "parent_id": 0, "navigate_url": "functions/availability/Doctor.php", "sort_number": 35, "role_number": 3})
        root_list.append({"id": 63, "name": "Issues", "icon": "icon-provider-issues", "parent_id": 0, "navigate_url": "functions/issues/index.php", "sort_number": 40, "role_number": 3})

        root_list.append({"id": 64, "name": "Facility Profile", "icon": "", "parent_id": 0, "navigate_url": "functions/user/facility_profile.php", "sort_number": 22, "role_number": 2})
        root_list.append({"id": 65, "name": "Redianswer Credentialing", "icon": "icon-facility-redianswer-credenitaling", "parent_id": 0, "navigate_url": "functions/user/RediAnswerCredentialing.php", "sort_number": 23, "role_number": 2})

        root_list.append({"id": 66, "name": "New Patient", "icon": "icon-provider-new-patient", "parent_id": 0, "navigate_url": "functions/medical_forms/OnCallInput.php?inputType=AdmitStaff", "sort_number": 45, "role_number": 3})
        root_list.append({"id": 67, "name": "Records", "icon": "icon-provider-records", "parent_id": 0, "navigate_url": "functions/records/inputRecord.php", "sort_number": 50, "role_number": 3})
        root_list.append({"id": 68, "name": "Recycle", "icon": "icon-provider-recycle", "parent_id": 0, "navigate_url": "functions/records/inputRecycle.php", "sort_number": 55, "role_number": 3})
        root_list.append({"id": 69, "name": "My Template", "icon": "icon-provider-my-template", "parent_id": 0, "navigate_url": "functions/my_template/MyTemplate.php", "sort_number": 60, "role_number": 3})
        root_list.append({"id": 70, "name": "Archive", "icon": "", "parent_id": 0, "navigate_url": "functions/faxlist/index.php", "sort_number": 65, "role_number": 3})
        root_list.append({"id": 71, "name": "Records", "icon": "icon-provider-records", "parent_id": 0, "navigate_url": "functions/records/billingRecords.php", "sort_number": 70, "role_number": 3})
        root_list.append({"id": 72, "name": "Records", "icon": "icon-provider-records", "parent_id": 0, "navigate_url": "functions/attending_records/index.php", "sort_number": 75, "role_number": 3})
        #for test
        root_list.append({"id": 73, "name": "test_node", "icon": "icon-provider-records", "parent_id": 7, "navigate_url": "functions/attending_records/index.php", "sort_number": 75, "role_number": 1})
        return root_list

    @staticmethod
    def get_functions():
        function_lists = filter(lambda x: x['role_number'] == 1,Permission.get_function_array())
        for i in function_lists:
            i['childs'] = []
        function_lists = sorted(function_lists, key=lambda x:x['sort_number'])
        function_lists = [{k: v for k, v in d.iteritems() if k
                     in ['id', 'name', 'navigate_url', 'icon', 'parent_id', 'childs']}
                     for d in function_lists]
        sort_order = ['id', 'name', 'icon', 'parent_id', 'navigate_url', 'childs']
        function_lists = [OrderedDict(sorted(item.iteritems(), key=lambda (k, v): sort_order.index(k)))
                          for item in function_lists]
        result_menu = filter(lambda x: x['parent_id'] == 0, function_lists)
        for node in result_menu:
            Permission.get_root_merge(node, function_lists)

        for j in result_menu:
            print json.dumps(j)

    @staticmethod
    def get_root_merge(node, function_lists):
        sort_order = ['id', 'name', 'icon', 'parent_id', 'navigate_url', 'childs']
        childs = filter(lambda x: x['parent_id'] == node['id'], function_lists)
        for n in childs:
            Permission.get_root_merge(n, function_lists)
        node['childs'] = childs


Permission.get_functions()

