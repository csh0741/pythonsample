#encoding:utf-8
import pytz
from datetime import datetime, timedelta
import time

foo = [2, 18, 9, 22, 17, 24, 8, 12, 27]
print filter(lambda x: x % 3 == 0, foo)  #將原來的組合過濾為3的倍數

map(lambda x: x * 2 + 10, foo)

print map(lambda x: x * 2 + 10, foo)   #將原來的組合乘2加10

print reduce(lambda x, y: x + y, foo)  #將原來的組合處理合計

print("'1234555'"+"1234")

for tz in pytz.all_timezones:
    print(tz)


eastern = pytz.timezone('US/Eastern')
loc_dt = eastern.localize(datetime(2015,06,01, 6, 0, 0))
fmt = '%Y-%m-%d %H:%M:%S %Z%z'
print(loc_dt.strftime(fmt))


# METHOD 1: Hardcode zones:
from_zone = pytz.timezone('UTC')
to_zone = pytz.timezone('America/Chicago')
date1 = datetime.now().replace(tzinfo=pytz.utc).astimezone(to_zone)
print date1
utc = pytz.utc

utc_dt = datetime.now().replace(tzinfo=utc)
print utc_dt

local_dt=datetime.now()
print local_dt

chicago_dt = datetime.now().replace(tzinfo=utc).astimezone(to_zone)
print chicago_dt
print chicago_dt.strftime("%H:%M:%S %Z")
local_std_date = to_zone.localize(local_dt,True)
print local_std_date

actionArray = ['Assign', 'EnterRoom', 'OnVideo', 'OnWebRTC', 'OnSkype', 'PtEnterSkype'
               , 'PtEnterFaceTime', 'DrEnterFaceTime', 'RTCReady', 'PtEnter', 'ClosePT', 'Modify', 'Saved']
action = "Assign"


if(actionArray.count(action) > 0):
    print "has.."+action

if action in actionArray:
    print "has.."+action

for name in actionArray[0:3]:
    print name